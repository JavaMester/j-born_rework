package s02;

import java.util.Scanner;

//С клавиатуры вводится расстояние в сантиметрах - N. Найти число полных метров в нем.
public class Task01 {
    public static void main(String[] args) {
        int number = requestNumber();
        int miters = number/100;
        System.out.println("Полное количество метров в введенных сантиметрах = " + miters);
    }


    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество сантиметров:");
        return scanner.nextInt();
    }
}