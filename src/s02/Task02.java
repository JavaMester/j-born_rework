package s02;

import java.util.Scanner;

//С начала суток прошло N секунд (N - вводится с клавиатуры). Определить:
//а) сколько полных часов прошло с начала суток;
//б) сколько полных минут прошло с начала очередного часа;
//в) сколько полных секунд прошло с начала очередной минуты.
public class Task02 {

    static int number = requestNumber();
    static int hours = number/3600;
    static int minuts = (number%3600)/60;
    static int seconds = number%60;

    public static void main(String[] args) {
        System.out.println("С начала суток прошло: " + hours + " часов " + minuts + " минут " + seconds + " секунд.");
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество секунд:");
        return scanner.nextInt();
    }
}