package s02;

import java.util.Scanner;

//Дано двузначное число - N (N - вводится с клавиатуры). Найти:
//а) число десятков в нем;
//б) число единиц в нем;
//в) сумму его цифр;
//г) произведение его цифр.
public class Task03 {
    static int number = requestNumber();
    static int tens = number/10;
    static int ones = number%10;
    static int sum = tens+ones;
    static int pro = tens*ones;

    public static void main(String[] args) {
        System.out.println(
                "В числе " + tens + " десятков, " + ones + " единиц, "
                        + sum + " сумма этих чисел " + pro + " произведение этих чисел.");
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите двузначное число:");
        return scanner.nextInt();
    }
}