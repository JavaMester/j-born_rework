package s02;

import java.util.Scanner;

//Известны координаты двух точек (x1, y1) и (x2, y2) - вводятся с клавиатуры. Составить программу вычисления расстояния между ними.
public class Task04 {
    static int first = requestNumber1();
    static int second = requestNumber2();
    static double s = Math.sqrt(((first/10 - first%10) * (first/10 - first%10))
            + ((second/10 - second%10) * (second/10 - second%10)));

    public static void main(String[] args) {
        System.out.println("Расстояние между точками: " + s);
    }

    static int requestNumber1() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите X и Y первой точки:");
        return scanner.nextInt();
    }

    static int requestNumber2() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите X и Y второй точки:");
        return scanner.nextInt();
    }
}