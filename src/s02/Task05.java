package s02;

import java.util.Scanner;

//С клавиатуры вводится три целых числа X, Y, Z. Определить являются ли эти числа тройкой Пифагора.
// Изначально неизвестно какое из чисел гипотенуза.
public class Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число X:");
        int x = scanner.nextInt();
        System.out.println("Введите число Y:");
        int y = scanner.nextInt();
        System.out.println("Введите число Z:");
        int z = scanner.nextInt();

        if ((x*x + y*y == z*z) || (y*y + z*z == x*x) || (x*x + z*z == y*y)) {
            System.out.println("Введенные числа являются Тройкой Пифагора.");
        } else {
            System.out.println("Введенные числа не являются Тройкой Пифагора.");
        }
    }
}