package s02;

import java.util.Scanner;

//Написать функцию, которая считает площадь круга, а также длину окружности по его радиусу - R (вводится с клавиатуры).
public class Task06 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите радиус: ");
        int r = sc.nextInt();
        System.out.println("Вы ввели радиус: " + r + " см.");
        System.out.println("Площадь круга: " + Math.PI*(r*r) + " см.");
        System.out.println("Длина окружности: " + 2*Math.PI*r + " см.");
    }
}