package s02;

import java.util.Scanner;

//Рассчитать значение Y при заданном значении X (X - вводится с клавиатуры).
// Синус рассчитывать методом Math.sin(double a)
public class Task07 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число X, что бы рассчитать число Y:");
        int x = sc.nextInt();
        double y;

        if (x > 0) {
            y = Math.sin(2*x);
        } else {
            y = 1 - 2*Math.sin(x*x);
        }
        System.out.println("Синус Y = " + y);
    }
}