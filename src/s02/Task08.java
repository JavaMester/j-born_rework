package s02;

import java.util.Scanner;

//С клавиатуры вводится число - N, необходимо написать программу,
// которая выводит таблицу умножения чисел от 1 до 10 на заданное число.
public class Task08 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число для вычисления таблицы умножения заданного числа:");
        int a = sc.nextInt();

        for (int i = 1; i <= 10; i++) {
            System.out.println(a + " * " + i + " = " + (a*i));
        }
    }
}