package s02;

import java.util.Scanner;

//Дано число произвольной длины - N (вводится с клавиатуры). Найти сумму его цифр.
public class Task09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        long x = scanner.nextInt();
        long sum = 0;

        while (x > 0) {
            sum += x % 10;
            x /= 10;
        }

        System.out.println("Сумма цифр введенного числа равна: " + sum);
    }

}