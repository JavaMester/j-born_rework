package s02;

import java.util.Scanner;

// Написать программу, которая вычисляет сумму всех четных элементов массива и
// произведение всех нечетных элементов массива (проверять четность и нечетность индекса).
// Размер массива и сам массив вводится с клавиатуры.
public class Task10 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размер массива");
        int arraySize = sc.nextInt();
        int[] a = new int[arraySize];

        System.out.println("Введите элементы массыва");

        for (int i = 0; i < arraySize; i++) {
            a[i] = sc.nextInt();
        }

        int sum = 0;
        int pro = 1;

        for (int i = 0; i < a.length; i++) {
            if (a[i] % 2 == 0) {
                sum += a[i];
            } else {
                pro *= a[i];
            }
        }

        System.out.println("Размер введенного массива = " + arraySize);
        System.out.print("Введенные числа: ");
        for (int j : a) {
            System.out.print(j + " ");
        }
        System.out.println();
        System.out.println("**********************************************");
        System.out.println("Произведение нечетных элементов массива = " + pro);
        System.out.println("Сумма четных элементов массива = " + sum);
    }
}