package s03;

import java.util.Scanner;

//Составить программу, которая запрашивает из консоли название государства и его столицы,
// а затем выводит сообщение: "Столица государства ... — город ..."
// (на месте многоточий должны быть выведены соответствующие значения).
public class Task01 {

    static String country;
    static String capitalOfCountry;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите страну: ");
        country = sc.nextLine();
        System.out.println("Введите столицу этой страны: ");
        capitalOfCountry = sc.nextLine();

        System.out.println("Страна: " + country + " Столица этой страны: " + capitalOfCountry);
    }
}