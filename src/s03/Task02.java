package s03;

import java.util.Scanner;

//Написать программу, которая выводит последовательность рандомных целых чисел (ПИН-код) по количеству регистров,
// которые вводятся из консоли. Например, N=7 (N-вводится из консоли). Результат: 5412409.
public class Task02 {

    static int n;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        n = scanner.nextInt();

        for (int i = 1; i <= n; i++) {
            System.out.print((int) (Math.random() * n));
        }
    }
}