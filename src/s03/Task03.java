package s03;

import java.util.Scanner;

//Вывести в консоле строки, состоящие из введенных символов. Строка вводится с клавиатуры. Например, слово «колбаса».
// Результат:
//к
//оо
//ллл
//бббб
//ааааа
//сссссс
//ааааааа
public class Task03 {

    static String word;
    static char[] wordToCharArray;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слово:");
        word = sc.nextLine();
        wordToCharArray = word.toCharArray();

        for (int i = 0; i < wordToCharArray.length; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(wordToCharArray[i]);
            }
            System.out.println();
        }
    }
}