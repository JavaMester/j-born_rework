package s03;

import java.util.Scanner;

//Нарисовать в консоли прямоугольник с 2-мя заданными сторонами. Стороны (целые числа) запрашиваются из консоли A и B.
// Например A=10 и B=5, то вывести:
//**********
//*        *
//*        *
//*        *
//**********
public class Task04 {

    static int a;
    static int b;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размер одной стороны прямоугольника:");
        a = sc.nextInt();
        System.out.println("Введите размер второй стороны прямоугольника:");
        b = sc.nextInt();
        char[][] sqr = new char[a][b];

        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                if (((i == 0) || (j == 0)) || ((i == a -1) || (j == b - 1))) {
                    sqr[i][j] = '$';
                } else {
                    sqr[i][j] = ' ';
                }
                System.out.print(sqr[i][j]);
            }
            System.out.println();
        }
    }
}