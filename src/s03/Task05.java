package s03;

import java.util.Scanner;

//Из консоли вводится строка. Определить является ли строка палиндромом (например: мадам, комок, ротор).
public class Task05 {

    static String str;

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слово для его проверки на палиндром:");
        str = sc.nextLine();
        StringBuilder newWord = new StringBuilder();

        for (int i = str.length() - 1; i >= 0; --i) {
            newWord.append(str.charAt(i));
        }

        if (str.equals(newWord.toString())) {
            System.out.println("Слово является палиндромом.");
        } else {
            System.out.println("Слово не является палиндромом.");
        }
    }
}