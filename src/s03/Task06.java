package s03;

import java.util.Scanner;

//Из консоли вводится строка и символ.
//Определить сколько раз встречается данный символ и заменить в строке этот символ на верхний регистр,
//например, str="java" и ch='a', то результат:
//Кол-во вхождений: 2
//Преобразованная строка: jAvA
public class Task06 {

    public static String word;
    public static String newWord;
    public static String letter;
    public static int count;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слово:");
        word = sc.nextLine();
        System.out.println("Введите букву:");
        letter = sc.nextLine();

        char ch = letter.charAt(0);

        count = 0;
        char[] wordToChar = word.toCharArray();

        for (int i = 0; i < word.length(); i++) {
            if (wordToChar[i] == ch) {
                count++;
                wordToChar[i] = Character.toUpperCase(ch);
            }
        }

        newWord = String.valueOf(wordToChar);

        System.out.println("Вы ввели слово: " + word);
        System.out.println("Вы ввели букву: " + letter);

        if (count == 0) {
            System.out.println("В введенном слове не встретилось ни одной введенной буквы.");
        } else {
            System.out.println("Количество букв в введенном слове: " + count);
            System.out.println("Предобразованное слово: " + newWord);
        }
    }
}