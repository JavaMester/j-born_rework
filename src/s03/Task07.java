package s03;

import java.util.Scanner;

//Из консоли вводится строка, содержащая символы верхнего и нижнего регистра.
//Символы верхнего регистра заменить на нижний и наоборот.
//Например:
//Строка: jBoRn
//Результат инверсии: JbOrN
public class Task07 {

    public static String word;                 // Объявляем статическую переменную
    public static String newWord;              // Объявляем статическую переменную

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); // Создаем Scanner для считывания символов из консоли
        System.out.println("Введите слово с буквами верхнего и нижнего регистра.");
        word = sc.nextLine();  // Записываем в переменную введенное из консоли слово, тем самым инициализируем её
        char[] wordToChar = word.toCharArray(); // Переводим введенное слово в массив char

        for (int i = 0; i < word.length(); i++) { // В цикле проходим по введенному слову
            char ch = word.charAt(i); // Переводим символы слова в char
            if (Character.isUpperCase(ch)) { // Если введенный символ уже в верхнем регистре
                ch = Character.toLowerCase(ch); // Переводим символ в нижний регистр
            } else { // В противном случае (если симво в нижнем регистре)
                ch = Character.toUpperCase(ch); // Переводим символ в верхний регистр
            }
            wordToChar[i] = ch; // Записываем каждый символ в массив
        }

        newWord = String.valueOf(wordToChar); // Берем новую переменную и записываем в нее результат переведенного слова
        System.out.println("Преобразованное слово: " + newWord); // Печатаем результат
    }
}