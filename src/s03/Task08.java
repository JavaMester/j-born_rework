package s03;

import java.util.Scanner;

//Инверсия строки. Из консоли вводится строка, реализовать ее инверсию. Не использовать метед reverse().
//Например:
//Строка: "Привет всем моим друзьям!"
//Результат инверсии: "!мяьзурд миом месв тевирП"
public class Task08 {

    public static String word;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слово или предложение:");
        word = sc.nextLine();

        char[] ch = word.toCharArray();

        System.out.println();
        System.out.print("Реверсивный вывод введенного слова или предложения: ");
        for (int i = word.length() - 1; i >= 0; i--) {
            System.out.print(ch[i]);
        }
    }
}