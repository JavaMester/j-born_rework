package s03;

import java.util.Arrays;
import java.util.Scanner;

//Из консоли вводится массив строк (вспомним как в первом занятии вводили массивы, сначала размер, потом по элементу),
//вывести все введенные строки через запятую. Учесть что на краях новой строки не должно быть запятых.
//Например:
//Введите кол-во строк: 3
//Строка 0: красный
//Строка 1: синий
//Строка 2: жельтый
//Результат: красный, синий, жельтый
public class Task09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество строк");

        int arraySize = scanner.nextInt();

        String[] array = new String[arraySize];

        System.out.println("Введите строки массива");

        for (int i = 0; i < array.length; i++) {
            System.out.print("Строк " + i + ": ");
            array[i] = scanner.next();
        }
        System.out.println("Результат: " + Arrays.toString(array));
    }
}