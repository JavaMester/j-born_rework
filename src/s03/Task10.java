package s03;

import java.util.Scanner;

//Из консоли вводится предложение. Определить ошибся ли автор в написании "Жи/Ши пиши с буквой и".
public class Task10 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите слово:");
        String word1 = scanner.nextLine();
        word1 = word1.toLowerCase();

        String gi = "жы";
        String shi = "шы";

        if (word1.contains(gi) || word1.contains(shi)) {
            System.out.println("Жи/Ши написано с ошибкой");
        } else {
            System.out.println("Все верно");
        }
    }
}