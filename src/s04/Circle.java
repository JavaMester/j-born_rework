package s04;

public class Circle {
    private int x;
    private int y;
    private final int r;

    Circle() {
        x = 3;
        y = 5;
        r = 7;
    }

    Circle(int x2, int y2, int r2) {
        x = x2;
        y = y2;
        r = r2;
    }

    public int getR() {
        return r;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public void moveCircle(int a, int b) {
        x += a;
        y += b;
    }

    public double squareOfCircle() {
        return Math.PI * r * r;
    }

    double distanceOfCenters(Circle circle2) {
        double xd = Math.pow((x) - circle2.getX(), 2);
        double yd = Math.pow((y - circle2.getY()), 2);
        Math.sqrt(xd + yd);
        return Math.sqrt(xd + yd);
    }

    boolean crossingCircles(Circle circle2) {
        return !((r + circle2.getR()) < this.distanceOfCenters(circle2))
                && (!(Math.abs(circle2.getR() - r) > this.distanceOfCenters(circle2)));
    }

}