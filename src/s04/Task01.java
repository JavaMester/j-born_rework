package s04;

//Реализовать героя (как в видео). Добавить ему getter на поля (узнать что это такое)
public class Task01 {
    public static void main(String[] args) {
        Hero scorpion = new Hero(60);
        Hero subZero = new Hero(40);

        System.out.println(subZero);
        scorpion.hit(subZero);
        System.out.println(subZero);
    }
}