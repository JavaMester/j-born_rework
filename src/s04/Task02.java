package s04;

import java.util.Scanner;

//Написать рекурсивный метод, который бы инвертировал строку.
//Инвертировать строку - переставить символы исходной строки в обратном порядке,
//например, для строки “jborn”, инверсия будет “nrobj”.
public class Task02 {

    public static void main(String[] args) {
        System.out.println("Введите слово или предлжение:");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();

        String reverse = reverseWord(str);

        System.out.println("Строка в обратном порядке: " + reverse);
    }

    public static String reverseWord(String str) {
        if (str.isEmpty()) {
            return str;
        }

        return reverseWord(str.substring(1)) + str.charAt(0);
    }
}