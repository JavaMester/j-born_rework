package s04;

import java.util.Scanner;

//Напишите рекурсивный метод вычисляющий n-ое число Фибоначчи.
public class Task03 {
    public static void main(String[] args) {
        System.out.println("Введите число: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(fibonachi(n));
    }

    private static int fibonachi(int i) {
        if (i == 0) {
            return 0;
        } else if (i == 1) {
            return 1;
        } else {
            return fibonachi(i - 2) + fibonachi(i - 1);
        }

    }
}