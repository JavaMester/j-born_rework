package s04;

//Создайте класс окружности Circle на координатной плоскости.
//Инициализация - радиус.
//Он должен уметь:
//Предоставлять площадь круга, ограниченного этой окружностью.
//Изменять положение центра окружности.
//Отвечать на вопрос, есть ли пересечения хотя бы в 1 точке с другой окружностью.
//Вычислять расстояние до другой окружности(расстояние между их центрами).
public class Task04 {
    public static void main(String[] args) {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2, 6, 4);

        System.out.println("Площадь круга с радиусом " + circle1.getR() + " равна " + circle1.squareOfCircle());
        System.out.println();

        System.out.println("Расстояние между центрами " + circle1.distanceOfCenters(circle2));
        System.out.println();

        if (circle1.crossingCircles(circle2)) {
            System.out.println("Окружности пересекаются");
        } else {
            System.out.println("Окружности не пересекаются");
        }

    }
}