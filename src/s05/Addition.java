package s05;

public class Addition extends Operation{
    @Override  // Переопределение метода вычисления под поставленную задачу сложения
    int calculate(int leftOperand, int rightOperand) {
        int result = leftOperand + rightOperand;
        Operation.setPrevious(result);
        return result;
    }

    @Override
    int returnPrevious(int previous) {
        return previous;
    }
}