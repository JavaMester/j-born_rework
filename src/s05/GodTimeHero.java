package s05;

import s04.Hero;

import java.util.Date;

public class GodTimeHero extends Hero {

    private final Date godModeTimeHero;

    private Date setGodTimeHero (int godMinutes) {
        return new Date(new Date().getTime() + godMinutes * 60000L);
    }

    public GodTimeHero(int power, int godMinutes) {
        super(power);
        this.godModeTimeHero = setGodTimeHero(godMinutes);
    }

    private boolean isGod() {
        return new Date().before(godModeTimeHero);
    }

    @Override
    public void setHealth(int health) {
        if (!isGod()) {
            super.setHealth(health);
        }
    }

    @Override
    public String toString() {
        return super.toString() +
                "is God \t" + isGod() + "\n";
    }

}