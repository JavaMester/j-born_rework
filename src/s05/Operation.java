package s05;

public abstract class Operation {

    private static int next;
    private static int prePrevious;

    abstract int calculate(int leftOperand, int rightOperand);

    abstract int returnPrevious(int previous);

    static void setPrevious(int next) {
        Operation.prePrevious = Operation.next;
        Operation.next = next;
    }
}