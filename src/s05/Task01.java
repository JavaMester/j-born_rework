package s05;

import s04.Hero;

//Реализовать героя под названием GodTimeHero.
//У которого есть аргумент конструктора - int godMinutes, кторый говорит о том,
//что герой неуязвим на протяжении godMinutes со времени создания героя.
public class Task01 {
    public static void main(String[] args) throws InterruptedException {
        Hero hero1 = new Hero(40);
        Hero hero2 = new GodTimeHero(30, 1);

        System.out.println(hero1);
        System.out.println(hero2);

        hero1.hit(hero2);
        System.out.println(hero2);

        Thread.sleep(70000);

        hero1.hit(hero2);
        System.out.println(hero2);

    }
}