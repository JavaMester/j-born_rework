package s05;

import java.util.Scanner;

//Создать базовый класс Operation. Этот класс будет представлять любую алгебраическую операцию.
//Он имеет следующие методы:
//int calculate(int leftOperand, int rightOperand) - сама операция
//и
//int returnPrevious() - возвращает предыдущий вычисленный результат.
//Поля определить самостоятельно.
//Создать двух наследников Addition и Multiplication для представления операций сложения и умножения соответственно.
//Переопределить метод calculate, предоставив подходящую по смыслу реализацию.
//В данной задаче имеет ли какой-нибудь смысл создавать экземпляры базового класса Operation?
public class Task02 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Введите первое значение: ");
        int a = sc.nextInt();

        System.out.println("Введите второе значение: ");
        int b = sc.nextInt();

        Operation multiplication = new Multiplication();
        System.out.println("Рузальтат умножения введенных данных: " + multiplication.calculate(a, b)
                             + "\n" + "Первое введенное значение: " + multiplication.returnPrevious(a));

        Operation addition = new Addition();
        System.out.println("Результат сложения введенных данных: " + addition.calculate(a, b)
                           + "\n" + "Первое введенное значение: " + addition.returnPrevious(a));
    }
}