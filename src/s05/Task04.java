package s05;

//Создать перечисление (Enum), описывающее латинский алфавит с методом, который бы возвращал номер буквы в алфавите.
public class Task04 {

    public static void main(String[] args) {
        gettingLetterOfLatinAlphabet(LatinAlphabet.L);
    }

    static void gettingLetterOfLatinAlphabet(LatinAlphabet latinAlphabet) {
        System.out.println("Номер введенной буквы в алфавите " + latinAlphabet.getNumber());
    }
}