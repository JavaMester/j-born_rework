package s06;

public class AlphabetHandler implements Handler {
    @Override
    public String handleMessage(String message) {
        if (message != null) {
            int mes1 = message.replaceAll("\\w", "").length();
            int mes2 = message.replaceAll("\\s", "").length();
            if ((mes1 + mes2) == message.length()) {
                return message;
            } else return null;
        }
        return null;
    }
}