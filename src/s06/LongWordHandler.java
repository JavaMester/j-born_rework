package s06;

public class LongWordHandler implements Handler {

    private final int m;
    private final int n;

    public LongWordHandler(int m, int n) {
        this.m = m;
        this.n = n;
    }

    @Override
    public String handleMessage(String message) {
        if (message != null) {
            int s = 0;
            String[] d = message.split(" ");
            for (String aD : d) {
                if (aD.length() >= n) {
                    s += 1;
                }
            }
            if (s >= m) {
                return message;
            } else return null;
        }
        return null;
    }
}