package s06;

public class RailwayCarriage {
    private final double carriage;

    public RailwayCarriage(double carriage) {
        this.carriage = carriage;
    }

    public double getCarriage() {
        return carriage;
    }
}