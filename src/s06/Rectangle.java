package s06;

public class Rectangle extends Shape {

    private final double sideA;
    private final double sideB;

    public Rectangle(double sideA, double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    @Override
    public double calculatePerimeter() {
        return ((sideA * 2) + (sideB * 2));
    }

    @Override
    public double calculateArea() {
        return sideA * sideB;
    }
}