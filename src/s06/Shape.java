package s06;

public abstract class Shape {
    public abstract double calculatePerimeter();
    public abstract double calculateArea();

    static double sumOfFigureArea(Shape[] arrayOfFigure) {
        double sum = 0;
        for (Shape sumArrayOfFigure : arrayOfFigure) {
            sum += sumArrayOfFigure.calculateArea();
        }
        return sum;
    }
}