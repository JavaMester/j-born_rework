package s06;

//Абстрактный класс. Написать абстрактный класс Shape, описывающий абстрактную геометрическую фигуру,
//с методами calculatePerimeter(), вычисляющим периметр фигуры, и calculateArea(), вычисляющим площадь фигуры.
//Реализовать три конкретных наследника: Square (квадрат), Rectangle (прямоугольник), Triangle (треугольник).
//Обратить внимание на наследование классов.
public class Task01 {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(3, 5, 6);
        Rectangle rectangle = new Rectangle(5, 8);
        Square square = new Square(3);

        System.out.println("Периметр треугольника: " + triangle.calculatePerimeter());
        System.out.println("Площадь треугольника: " + triangle.calculateArea());
        System.out.println("Периметр прямоугольника: " + rectangle.calculatePerimeter());
        System.out.println("Площадь прямоугольника: " + rectangle.calculateArea());
        System.out.println("Периметр квадрата: " + square.calculatePerimeter());
        System.out.println("Площадь квадрата: " + square.calculateArea());
    }
}