package s06;

//Полиморфизм. На основе Задачи №1 написать статический метод,
//который бы принимал массив геометрических фигур (квадрат, прямоугольник, треугольник) и возвращал суммарную площадь.
public class Task02 {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(3, 5, 6);
        Square square1 = new Square(4);
        Rectangle rectangle1 = new Rectangle(4, 9);

        Shape[] array = {triangle1, square1, rectangle1};
        System.out.println(triangle1.calculateArea());

        System.out.println("Сумма всех площадей фигур: " + Shape.sumOfFigureArea(array));
    }
}