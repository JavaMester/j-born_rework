package s06;

//Агрегирование. Написать класс Железнодорожного состава (группа сцепленных между собой вагонов),
//переиспользуя класс Вагона. Вагон характеризуется грузовместимостью (масса, тонна).
//Класс Железнодорожного состава должен предоставлять информацию об общей грузовместимости и
//средней грузовместимости на вагон.
public class Task03 {
    public static void main(String[] args) {
        RailwayCarriage[] railwayCarriages = new RailwayCarriage[17];

        for (int i = 0; i < railwayCarriages.length; i++) {
            double startMass = 10 + i;
            RailwayCarriage rc = new RailwayCarriage(startMass);
            railwayCarriages[i] = rc;
        }

        Train train = new Train(railwayCarriages);

        System.out.println("Средняя грузоподъемность вагонов состава: " + train.middleCapacity());
        System.out.println("Максимальная грузоподъемность вагонов состава: " + train.maxCapacity());
    }
}