package s06;

//У вас есть интерфейс Handler с методом handleMessage(String message), возвращающим String.
//Написать три имплементации:
//LowerCaseHandler: приводит исходное сообщение к нижнему регистру и убирает пробелы из начала и конца сообщения.
//AlphabetHandler: возвращает исходное сообщение, если оно состоит из буквенных символов и
//пробелов (пробелов может и не быть), иначе null.
//LongWordHandler: проверяет сообщение на количество слов (группа символов, разделённая пробелом)
//не менее m с длиной не менее n (эти параметры задаются в конструкторе), если проверка завершилась успешно,
//то возвращается строка без изменений, иначе null.
//таким образом, чтобы их можно было соединить их в цепочку.
//Реализовать последовательность обработки 1 > 2 > 3. Проконтролировать NullPointerException.
public class Task04 {
    public static void main(String[] args) {
        String messageLower = "    Check THIS Message     ";
        LowerCaseHandler lowerCaseHandler = new LowerCaseHandler();
        System.out.println(lowerCaseHandler.handleMessage(messageLower));

        String messageAlpha = "W hat is This 1 2 3";
        AlphabetHandler alphabetHandler = new AlphabetHandler();
        System.out.println(alphabetHandler.handleMessage(messageAlpha));

        String messageLong = "this is just a message";
        LongWordHandler longWordHandler = new LongWordHandler(2, 4);
        System.out.println(longWordHandler.handleMessage(messageLong));

        String messageAll = messageLower + messageAlpha + messageLong;
        String test = longWordHandler.handleMessage(alphabetHandler.handleMessage(lowerCaseHandler.handleMessage(messageAll)));
        System.out.println(test);
    }
}