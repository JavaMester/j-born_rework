package s06;

public class Train {

    private final RailwayCarriage[] railwayCarriages;

    Train(RailwayCarriage[] carriages) {
        this.railwayCarriages = carriages;
    }

    public double maxCapacity() {
        double maxC = 0;
        for (RailwayCarriage railwayCarriage : this.railwayCarriages) {
            maxC += railwayCarriage.getCarriage();
        }
        return maxC;
    }

    public double middleCapacity() {
        double midC = 0;
        for (RailwayCarriage railwayCarriage : this.railwayCarriages) {
            midC += railwayCarriage.getCarriage();
        }
        return midC / this.railwayCarriages.length;
    }
}