package s06;

public class Triangle extends Shape {

    private final double sideA;
    private final double sideB;
    private final double sideC;

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    private double halfOfPerimeter() {
        return (calculatePerimeter() / 2);
    }

    @Override
    public double calculatePerimeter() {
        return sideA+sideB+sideC;
    }

    @Override
    public double calculateArea() {
        return (Math.sqrt(halfOfPerimeter()
                * (halfOfPerimeter() - sideA)
                * (halfOfPerimeter() - sideB)
                * (halfOfPerimeter() - sideC)));
    }
}