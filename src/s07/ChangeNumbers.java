package s07;

import java.math.BigDecimal;
import java.util.Comparator;

public abstract class ChangeNumbers implements Comparator<Number> {

    static <T extends Number> int changeNumbers(Number i, Number j) {
        return new BigDecimal(i.toString()).compareTo(new BigDecimal(j.toString()));
    }

}