package s07;

public class Convertation implements ConvertPersons <Pilot, Programmer> {
    @Override
    public Pilot convertation(Programmer obj) {
        Pilot pi = new Pilot();

        pi.setAge(obj.getSalary());
        pi.setFirstName(obj.getFirstName());
        pi.setLastName(obj.getLastName());

        return pi;
    }
}
