package s07;

public class Driver {
    private final String firstName;
    private final String secondName;
    private final String fathersName;
    private final String dateOfBirth;
    private final String driveCardNumber;
    private final String driverCardSeries;
    private final String driverCarsDateOfOut;

    public Driver(String firstName, String secondName, String fathersName, String dateOfBirth,
                  String driveCardNumber, String driverCardSeries, String driverCarsDateOfOut) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.fathersName = fathersName;
        this.dateOfBirth = dateOfBirth;
        this.driveCardNumber = driveCardNumber;
        this.driverCardSeries = driverCardSeries;
        this.driverCarsDateOfOut = driverCarsDateOfOut;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getFathersName() {
        return fathersName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getDriveCardNumber() {
        return driveCardNumber;
    }

    public String getDriverCardSeries() {
        return driverCardSeries;
    }

    public String getDriverCarsDateOfOut() {
        return driverCarsDateOfOut;
    }
}