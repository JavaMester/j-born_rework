package s07;

public class DriversValidator implements Validator<Driver> {
    @Override
    public boolean isValid(Driver obj) {
        return !obj.getFirstName().isEmpty() || !obj.getSecondName().isEmpty() || !obj.getFathersName().isEmpty()
                || !obj.getDateOfBirth().isEmpty() || !obj.getDriveCardNumber().isEmpty()
                || !obj.getDriverCardSeries().isEmpty() || obj.getDriverCarsDateOfOut().isEmpty();
    }
}