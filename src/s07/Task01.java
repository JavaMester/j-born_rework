package s07;

//Написать generic interface Validator, где дженерик-тип, тип, который валидируется boolean isValid(T obj).
//Написать валидатор для класса Driver (имя, фамилия, отчетсов, дата рождения, серия и номер прав, дата выдачи),
//который полностью валидировал свою сущность.
public class Task01 {
    public static void main(String[] args) {
        Driver driver = new Driver("Igor", "Ivanov", "Olegovich",
                "01.01.1975", "1221", "123456", "01.01.1993");

        Validator<Driver> validator = new DriversValidator();

        System.out.println(validator.isValid(driver));
    }
}