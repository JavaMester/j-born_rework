package s07;

//Написать интерфейс конвертера, который способен конвертировать любые типы.
//Написать один из вариантов реализации.
//Например конвертация из сущности Driver в сущность Person (которая не содержит информацию о правах).
public class Task02 {
    public static void main(String[] args) {
        Convertation con = new Convertation();
        Programmer programmer = new Programmer("Pilot name - Mike", "Pilot last name - Robbinson", 54);
        System.out.println(con.convertation(programmer));
    }
}