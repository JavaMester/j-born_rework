package s07;

//Написать метод, которые сравнивает 2 любых числа (ограничение типов, класс Number) и
//возвращает положительное число - если первое число больше второго,
//0 - если числа равны, отрицательное число - если первое число меньше второго.
public class Task03 {
    public static void main(String[] args) {
        Integer integer1 = 34;
        Integer integer2 = 76;
        Long long1 = 178L;
        Double double1 = 34.3;
        Double double2 = 34d;

        System.out.println("Первое число меньше второго " +
                ChangeNumbers.changeNumbers(integer1, long1));
        System.out.println("Первое число больше второго " +
                ChangeNumbers.changeNumbers(integer2, integer1));
        System.out.println("Числа равны " + ChangeNumbers.changeNumbers(double2, integer1));
        System.out.println("Первое число больше второго " + ChangeNumbers.changeNumbers(double1, integer1));
    }
}