package s08;

import java.io.*;
import java.math.BigInteger;

public class Task01 {
    public static void main(String[] args) {

        try {
            readSumOfNumbers();
        } catch (FileNotFoundException e) {
            if (e.getMessage().contains("Can not find file")) {
                System.out.println("No such file");
            } else if (e.getMessage().contains("Access denied")) {
                System.out.println("Reading is not possible");
            }
        } catch (WrongFormatFileException e) {
            System.out.println("Invalid file format");
        } catch (WrongNumberFormatException e) {
            System.out.println("Invalid number format");
        } catch (NumberFormatException e) {
            System.out.println("The amount exceeds the maximum value Long.MAX_VALUE");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void readSumOfNumbers() throws IOException, WrongFormatFileException, WrongNumberFormatException {
        String inputFile = "C:\\Users\\User\\IdeaProjects\\J-Born_rework\\src\\s08\\ReadFileTask01";
        String outputFile = "C:\\Users\\User\\IdeaProjects\\J-Born_rework\\src\\s08\\RecordFileTask1";

        File input = new File(inputFile); // Файл для чтения сразу передаем объекту
        File output = new File(outputFile); // Файл для записи сразу передаем объекту
        BigInteger sum = BigInteger.valueOf(0);

        String firstLine = readFirstLine(input);

        if (!firstLine.matches("(\\S+\\s)+\\S+")) {
            throw new WrongFormatFileException();
        }
        if (!firstLine.matches("(\\d+\\s)+\\d+")) {
            throw new WrongNumberFormatException();
        }

        String[] numbers = firstLine.split(" "); // Парсим строку по пробелу, что бы потом посчитать числа
        long[] num = new long[numbers.length];

        for (int i = 0; i < numbers.length; i++) { // Переводим содержимое файла из строки в числа
            num[i] = Long.parseLong(numbers[i]);
        }

        for (long i : num) {
            sum = sum.add(BigInteger.valueOf(i));
        }
        if (sum.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0) {
            throw new NumberFormatException();
        }

        writeToFirst(output, sum.toString());
    }

    // Метод читает из файла
    private static String readFirstLine(File file) throws IOException {
        FileInputStream stream = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String first = reader.readLine();
        reader.close();
        stream.close();
        return first;
    }

    // Метод записывает строку в файл
    private static void writeToFirst(File file, String value) throws IOException {
        FileWriter writer = new FileWriter(file, false);
        writer.write(value);
        writer.flush();
        writer.close();
    }

    private static class WrongFormatFileException extends Throwable { // Создаем свое исключение
    }

    private static class WrongNumberFormatException extends Throwable { // Создаем свое исключение
    }
}