package s09;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

//Реализовать рекурсивный поиск в каталоге по строке названия файла (проверять на вхождение),
//используя классы Path и Files. Вернуть массив объектов Path.
public class Task01 {
    public static void main(String[] args) {
        ArrayList<Path> result = new ArrayList<>();
        Path path = Paths.get("C:\\Users\\User\\IdeaProjects\\J-Born_rework\\src\\s02\\Task01.java");
        findFile(path, "someFileName.txt", result);
    }

    private static Path findFile(Path pathName, String fileName, ArrayList<Path> result) {
        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(pathName)) {
            for (Path path : dirStream) {
                if (Files.isDirectory(path)) {
                    findFile(path, fileName, result);
                } else {
                    if (path.getFileName().toString().equals(fileName)) {
                        result.add(path);
                        System.out.println(path);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pathName;
    }
}