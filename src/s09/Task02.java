package s09;

import java.io.*;

//Записать в файл numbers поочерёдно 100 положительных и 100 отрицательных случайных целых чисел,
//перечислив их через пробел. Затем прочитать этот файл и раскидать прочитанные числа в 2 файла:
//positive_numbers и negative_numbers, с положительными и отрицательными числами соответственно.
public class Task02 {
    public static void main(String[] args) throws IOException {
        recordNumbers();
        readFromFileRecordToAnotherFiles();
    }

    //Статические переменные, что бы не мешались в коде
    static String ONE_WAY = "C:\\Users\\User\\IdeaProjects\\J-Born_rework\\src\\s09\\numbers";
    static String POSITIVE_WAY = "C:\\Users\\User\\IdeaProjects\\J-Born_rework\\src\\s09\\positive_numbers";
    static String NEGATIVE_WAY = "C:\\Users\\User\\IdeaProjects\\J-Born_rework\\src\\s09\\negative_numbers";

    // Метод записывает последовательно положительные и отрицательные числа рандомно
    private static void recordNumbers() throws IOException {
        try (Writer writer = new FileWriter(ONE_WAY)) {
            for (int i = 0; i <= 10; i++) {
                // Запись положительных чисел
                writer.write(((int) (Math.random() * 10) + 1) + " ");
                // Запись отрицательных чисел
                writer.write(((int) (Math.random() * 10) - 10) + " ");
            }
        }
    }

    private static void readFromFileRecordToAnotherFiles() throws IOException {
        try (Writer writerPositive = new FileWriter(POSITIVE_WAY);
             Writer writerNegative = new FileWriter(NEGATIVE_WAY);
             BufferedReader reader = new BufferedReader(new FileReader(ONE_WAY))) {

            // Читаем из файла, в который записали
            String number = reader.readLine();
            // Убираем не нужные пробелы между цыфрами и записываем результат в массив
            String[] arrayLines = number.split(" ");

            // Итерируемся по массиву
            for (String arrayLine : arrayLines) {
                // Если число оказывается больше 0, то оно положительное -> запись в файл положительных чисел
                if (Integer.parseInt(arrayLine) > 0) {
                    writerPositive.write(arrayLine + "\n"); // + перевод строки для записи в столбик
                    // В противном случае -> запись в файл отрицательных чисел
                } else {
                    writerNegative.write(arrayLine + "\n"); // + перевод строки для записи в столбик
                }
            }
        }
    }
}