package s09;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

//Напишите метод, который прочитает данные из InputStream и преобразует их в строку, используя заданную кодировку.
//Сигнатура метода должна выглядеть так:
//String readAsString(InputStream inputStream, Charset charset) throws IOException.
public class Task03 {
    public static void main(String[] args) throws IOException {
        System.out.println("JBorn 9-3");
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(new byte[]{48, 49, 50, 51})) {
            String str = readAsString(inputStream);
            System.out.println(str);

            String text = "Try to do 2";
            byte[] bytes = text.getBytes(StandardCharsets.UTF_8);
            System.out.println(Arrays.toString(bytes));
            // Return String from bytes
            String origin = new String(bytes, StandardCharsets.UTF_8);
            System.out.println(origin);
        }

    }

    private static String readAsString(InputStream inputStream) throws IOException {
        String reReturn;
        Reader reader = new InputStreamReader(inputStream, StandardCharsets.US_ASCII);
        StringWriter writer = new StringWriter();
        int a;
        while ((a = reader.read()) != -1) {
            writer.write(a);
        }
        reReturn = writer.toString();
        return reReturn;
    }
}