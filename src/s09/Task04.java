package s09;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

//Используя java.io.Externalizable создать вручную сериализацию класса (модифицировать класс) User(firstName, lastName):
//public class User {
//    private String firstname;
//    private String lastname;
//
//    public User(String firstname, String lastname) {
//        this.firstname = firstname;
//        this.lastname = lastname;
//    }
//
//   public String getFirstname() {
//        return firstname;
//   }
//
//   public String getLastname() {
//        return lastname;
//   }
//
//  @Override
//  public String toString() {
//      return "User{" +
//           "firstname='" + firstname + '\'' +
//           ", lastname='" + lastname + '\'' +
//           '}';
//  }
//}
//Обратите внимание на конструктор по-умолчанию при экстернализации.
public class Task04 {
    public static void main(String[]args) throws Exception {

        User user = new User("Sergei", "Golidonov");
        try (FileOutputStream fos = new FileOutputStream(
                "C:\\Users\\User\\IdeaProjects\\J-Born_rework\\src\\s09\\user.byte")) {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(user);
        }
    }

}